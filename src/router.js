import gql from "graphql-tag";
import { createRouter, createWebHistory } from "vue-router";
import { ApolloClient, createHttpLink, InMemoryCache } from '@apollo/client/core'
/*import App from './App.vue';*/

import LogIn from './components/LogIn.vue'
import SignUp from './components/SignUp.vue'
import HomeLandingP from './components/HomeLandingP.vue'
import HomeUser from './components/HomeUser.vue'
import HomeComoCertificarte from './components/HomeComoCertificarte.vue'
import HomeCursos from './components/HomeCursos.vue'
import HomeQuienesSomos from './components/HomeQuienesSomos.vue'
import Account from './components/Account.vue'
import AccountEditar from './components/AccountEditar.vue'
/*import CursoContenido from './components/CursoContenido.vue'
import CursoVideo from './components/CursoVideo.vue'*/



const routes = [/*{
        path: '/',
        name: 'root',
        component: App,
        meta: { requiresAuth: false }
    },*/
    {
        path: '/user/logIn',
        name: "logIn",
        component: LogIn,
        meta: { requiresAuth: false }
    },
    {
        path: '/user/HomeLandingP',
        name: "HomeLandingP",
        component: HomeLandingP,
        meta: { requiresAuth: false }
    },
    {
        path: '/user/Certificate',
        name: "HomeComoCertificarte",
        component: HomeComoCertificarte,
        meta: { requiresAuth: false }
    },
    {
        path: '/user/QuienesSomos',
        name: "HomeQuienesSomos",
        component: HomeQuienesSomos,
        meta: { requiresAuth: false }

    },
    {
        path: '/user/Cursos',
        name: "HomeCursos",
        component: HomeCursos,
        meta: { requiresAuth: false }
    },
    {
        path: '/user/signUp',
        name: "signUp",
        component: SignUp,
        meta: { requiresAuth: false }
    },
    {
        path: '/user/homeUser',
        name: "homeUser",
        component: HomeUser,
        meta: { requiresAuth: true }
    },
    {
        path: '/user/account',
        name: "account",
        component: Account,
        meta: { requiresAuth: true }
    },
    {
        path: '/user/AccountEditar',
        name: "AccountEditar",
        component: AccountEditar,
        meta: { requiresAuth: true }
    }
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

const apolloClient = new ApolloClient({
    link: createHttpLink({ uri: 'https://maieutics-api-gateway.herokuapp.com/' }),
    cache: new InMemoryCache()
})

async function isAuth() {
    if (localStorage.getItem("token_access") === null ||
    localStorage.getItem("token_refresh") === null) {
        return false;
    }

    try {
        var result = await apolloClient.mutate({
            mutation: gql `
                mutation ($refresh: String!) {
                    refreshToken(refresh: $refresh) {
                        access
                    }
                }
            `,
            variables: {
                refresh: localStorage.getItem("token_refresh"),
            },
        })
        
        localStorage.setItem("token_access", result.data.refreshToken.access);
        return true;
     } catch {
        localStorage.clear();
        alert("Su sesión expiró, por favor vuelva a iniciar sesión");
        return false;
     }
}

router.beforeEach(async(to, from) => {
    var is_auth = await isAuth();
    
    if (is_auth == to.meta.requiresAuth) return true
    if (is_auth) return { name: "homeUser" };
    return { name: "logIn" };
})

export default router;